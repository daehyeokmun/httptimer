FROM golang:1.8-alpine

COPY . /go/src/gitlab.com/daehyeok/httptimer

RUN go install gitlab.com/daehyeok/httptimer

CMD ["httptimer"]

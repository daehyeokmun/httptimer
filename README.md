![build_badge](https://gitlab.com/daehyeokmun/httptimer/badges/master/build.svg)
![goreportcard_badge](https://goreportcard.com/badge/gitlab.com/daehyeokmun/httptimer)
# HTTP response time monitor
Gitlab Backend Developer, CI/CD Pipelines pre-interview question

## Install & Run
Simplest way with docker
```shell
$docker run -it --rm daehyeok/httptimer
```

Build with local Go

```shell
$go get -u gitlab.com/daehyeokmun/httptimer
$httptimer
```

## Usage
```
Usage of httptimer:
  -dur string
        Duration for running program (default "5m")
  -interval string
        Interval time between request (default "0.5s")
  -url string
        Target URL for get response time (default "https://gitlab.com")
```

### Screenshot
![screenshot](screenshot.gif)

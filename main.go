package main

import (
	"flag"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/gizak/termui"
)

const textBoxHeight = 30
const graphDotsMax = 200

var url *string
var runningTime, interval time.Duration
var startTime time.Time

func parseFlag() error {
	url = flag.String("url", "https://gitlab.com", "Target URL for get response time")
	var durStr = flag.String("dur", "5m", "Duration for running program")
	var intervalStr = flag.String("interval", "0.5s", "Interval time between request")
	flag.Parse()

	var err error
	runningTime, err = time.ParseDuration(*durStr)

	if err != nil {
		return err
	}

	interval, err = time.ParseDuration(*intervalStr)

	if err != nil {
		return err
	}
	startTime = time.Now().Round(time.Second)
	return nil
}

type uiWidget struct {
	progressBar               *termui.Gauge
	titleBox, urlBox, respBox *termui.Par
	lineChart                 *termui.LineChart
	lineDots                  []float64
	respTexts                 []string
	mu                        *sync.RWMutex
}

func (ui *uiWidget) initUI() error {
	err := termui.Init()
	if err != nil {
		return err
	}

	ui.respTexts = make([]string, textBoxHeight)
	//ui.lineDots = make([]float64, 0)

	ui.mu = new(sync.RWMutex)

	uiWidth := 100
	ui.titleBox = termui.NewPar("Simple HTTP Response Time Checker(Press Q for Quit)")
	ui.titleBox.Height = 1
	ui.titleBox.Width = uiWidth
	ui.titleBox.Y = 1
	ui.titleBox.Border = false

	ui.urlBox = termui.NewPar(*url)
	ui.urlBox.Height = 3

	ui.urlBox.Width = uiWidth
	ui.urlBox.Y = 2
	ui.urlBox.BorderLabel = "Target URL"

	ui.progressBar = termui.NewGauge()
	ui.progressBar.Percent = 0
	ui.progressBar.Width = uiWidth
	ui.progressBar.Height = 3
	ui.progressBar.Y = 5
	ui.progressBar.BorderLabel = "Running Time"
	ui.progressBar.Label = ""
	ui.progressBar.LabelAlign = termui.AlignRight

	ui.respBox = termui.NewPar("")
	ui.respBox.Height = textBoxHeight
	ui.respBox.Width = uiWidth
	ui.respBox.Y = 9
	ui.respBox.BorderLabel = fmt.Sprintf("Recent %d Response", textBoxHeight)
	ui.respBox.BorderFg = termui.ColorYellow

	ui.lineChart = termui.NewLineChart()
	ui.lineChart.BorderLabel = fmt.Sprintf("Recent %d Response Time (ms) Chart", graphDotsMax)

	ui.lineChart.Width = uiWidth
	ui.lineChart.Height = 15
	ui.lineChart.X = 0
	ui.lineChart.Y = 40
	ui.lineChart.AxesColor = termui.ColorWhite
	ui.lineChart.LineColor = termui.ColorGreen | termui.AttrBold

	termui.Handle("/sys/kbd/q", func(termui.Event) {
		termui.StopLoop()
	})

	ui.renderUI()
	return nil
}

func (ui *uiWidget) renderUI() {
	progressedTime := time.Now().Round(time.Second).Sub(startTime)
	percent := int(progressedTime.Seconds() / runningTime.Seconds() * 100)

	ui.progressBar.Percent = percent
	ui.progressBar.Label = fmt.Sprintf("%s / %s (%d%%)", progressedTime, runningTime, percent)

	ui.mu.RLock()
	ui.respBox.Text = strings.Join(ui.respTexts, "\n")
	ui.lineChart.Data = ui.lineDots
	ui.mu.RUnlock()

	termui.Render(ui.titleBox, ui.urlBox, ui.progressBar, ui.respBox, ui.lineChart)
}

func (ui *uiWidget) add(resp *respRes) {

	var line string

	if resp.err == nil {
		line = fmt.Sprintf("[%-15s](fg-yellow) Request : %s", resp.responseTime, resp.requestTime)
	} else {
		line = fmt.Sprintf("[%s](fg-red) Request : %s", resp.err, resp.requestTime)
	}

	ui.mu.Lock()
	defer ui.mu.Unlock()

	ui.respTexts = append([]string{line}, ui.respTexts[:(textBoxHeight-1)]...)
	if resp.err == nil {
		ui.lineDots = append(ui.lineDots, resp.responseTime.Seconds()*1000)
		if graphDotsMax < len(ui.lineDots) {
			ui.lineDots = ui.lineDots[len(ui.lineDots)-graphDotsMax:]
		}
	}

}

type respRes struct {
	requestTime  time.Time
	responseTime time.Duration
	err          error
}

func sendRequest(url string) *respRes {
	res := new(respRes)
	res.requestTime = time.Now()

	var resp *http.Response
	resp, res.err = http.Get(url)

	res.responseTime = time.Now().Sub(res.requestTime)

	if res.err == nil {
		resp.Body.Close()
	}

	return res
}

func main() {
	if err := parseFlag(); err != nil {
		panic(err)
	}

	if flag.Lookup("help") != nil {
		flag.PrintDefaults()
		return
	}

	ui := new(uiWidget)
	if err := ui.initUI(); err != nil {
		panic(err)
	}
	defer termui.Close()

	ticker := time.NewTicker(interval)
	quitTicket := time.NewTicker(runningTime)
	respCh := make(chan *respRes, 10)
	ui.renderUI()

	go func() {
		for {
			select {
			case <-ticker.C:
				go func() { respCh <- sendRequest(*url) }()
				ui.renderUI()
			case <-quitTicket.C:
				ticker.Stop()
				quitTicket.Stop()
			case resp := <-respCh:
				ui.add(resp)
				ui.renderUI()
			}
		}
	}()

	termui.Loop()
}
